+++
title = "Ft_ls"
categories = "Cursus 42"
type_content = "code"
preview = "/img/preview/ft_ls.png"
date = 2020-08-22T16:30:07+02:00
description = "Recoder la commande ls pour apprendre sur le systéme"
coders = []
status = ["done"]
gitlab = ["https://gitlab.com/husahuc/ft_ls"]
type = "post"
+++

C'est l'un des premiers gros projets que j'ai fait durant mon cursus. Recoder la commande ls est un travail d'organisation sur le code pour le finir correctement.

![ft_ls image](/img/ft_ls/ft_ls_1.png)

La partie la plus compliquée est l'option -R pour lister en récursion, voir les sous-dossiers d'un dossier. Aussi pour voir les fichiers spéciaux contenue notamment dans le dossier /dev et ses "devices". Et l'alignement pour l'affichage avec les nombres.

![ft_ls image](/img/ft_ls/ft_ls_2.png)

Il y a quelques erreurs dans le code, comme le sort avec des options multiples. Je pourrais aussi utiliser un algorithme plus rapide comme le Quick Sort.

![ft_ls image](/img/ft_ls/ft_ls_3.png)