+++
title = "Rte"
categories = "42 stages"
type_content = "code"
preview = "/img/preview/RTE-CDF.png"
date = 2020-08-22T18:00:57+02:00
description = "Suivre une aplication de suivi de formation"
type = "post"
[[tech]]
logo = "https://www.django-rest-framework.org/img/logo.png"
name = "Django rest framework"
url = "https://www.django-rest-framework.org"
[[tech]]
logo = "https://vuejs.org/images/logo.png"
name = "VueJS"
url = "https://vuejs.org"
[[tech]]
logo = "https://avatars1.githubusercontent.com/u/22965283?s=200&v=4"
name = "Bootstrap Vue"
url = "https://bootstrap-vue.org"
+++

J'ai fait mon premier stage pour le cursus de 42 à RTE, pour suivre une application de suivi de formation "Le cahier de formation".
RTE est responsable de la gestion, de la maintenance, et du développement du réseau a haute tension d'électricité en France.

RTE, due à ses spécialisations, a un grand pôle interne de formation avec 10 % de la masse salariale dédié.
Pour suivre les stagiaires, l'application "Le cahier de formation" a été mise en place. Elle permet pour les formateurs d'avoir une vision globale du niveau et de l'évolution des stagiaires. Pour les stagiaires, il y a plusieurs types d'évaluations, comme des mises en situation ou des Quiz.

Notre mission était de migrer l'application déjà en production dans une nouvelle version plus simple. Le back a été fait avec Django Rest Framework pour les appels d'API. Nous avons considéré Angular pour quelques semaines, pour son écosystème complet. Mais nous avons finalement utilisé VueJs, un framework plus simple, mais tout aussi puissant. J'ai ainsi pu voir les aspects les plus importants comme le routeur pour les pages, VueX pour stocker des variables. Bootstrap Vue a été utilisé pour le css, pour sa bonne documentation. J'ai fini par faire des tests unitaires avec Vue-Test-Utils pour s'assurer que l'application fonctionnait correctement.

Dus au Conoravirus, nous avons utilisé Teams pour avoir des réunions hebdomadaires. Nous avons aussi utilisé Git pour partager le code.

La version finale de l'application est désormais utilisée en production. Les utilisateurs sont contents du gain de fluidité. Le code est plus simple, et mieux segmenté, permettant un suivi plus facile.

La vidéo que j'ai faite pour présenter mon stage :

{{< dailymotion x7wd1pj >}}