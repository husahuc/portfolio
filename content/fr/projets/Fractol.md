+++
title = "Fractol"
categories = "42 Cursus"
type_content = "code"
preview = ""
date = 2020-08-22T16:30:07+02:00
description = "Fractales"
coders = []
status = ["done"]
gitlab = ["https://gitlab.com/husahuc/fractol"]
type = "post"
+++

Fract-ol est l'un des célèbres projets de 42, une bonne introduction à la programmation graphique et comment comprendre des mathématiques étranges.

Une fractale est un motif qui ne se termine jamais, souvent basé sur une formule mathématique. Ils sont autosimilaires à différentes échelles, ce qui signifie que vous pouvez zoomer et voir le même motif encore et encore. Cela peut être créé en répétant indéfiniment un processus simple dans une boucle infinie, en code avec la récursivité.

Vous pouvez afficher des images d'un fractal avec la partie réelle du nombre complexe sur le long de l'axe des x et la partie imaginaire sur l'axe y. Calculer pour chaque point le nombre d'itérations dans la séquence et vérifier s'il est fini ou infini, et le nombre d'itérations.

Il existe des options pour modifier la gamme de couleurs en fonction du nombre d'itérations, cela rend des effets psy.

Vous pouvez zoomer et cliquer pour centrer sur le pointeur. Il y a une option pour augmenter le nombre maximum d'itérations pour donner plus de détails aux formes. Le processus de calcul de chaque pixel prend beaucoup de ressources et vous pouvez diviser ce processus. D'où la nécessité du multithreading, calcul de plusieurs points avec des threads différents. J'ai choisi d'avoir 4 threads, chaque pixel de calcul dans chaque coin de l'écran en même temps.

**Mandelbrot** est l'ensemble des nombres complexes pour lesquels la fonction f(x) = z*2 + c ne diverge pas à l'infini lorsqu'elle est itérée à partir de f(x) = 0. Il peut être intéressant de voir chaque « coin » et le « cayon » pour obtenir un motif circulaire.

![mandelbroth image 1](/img/fractol/mandelbroth_1.png)

![mandelbroth image 2](/img/fractol/mandelbroth_2.png)


**Burning ship** est l'ensemble des valeurs absolues de Mandelbrot. Vous pouvez zoomer pour voir différents types de navire en feu.

![Burning ship image 1](/img/fractol/burning_ship_1.png)

**Julia** représente l'ensemble des entrées dont les sorties résultantes tendent vers l'infini ou restent bornées, dans l'opération z = z2 + c. Vous pouvez modifier les valeurs z et x en déplaçant le curseur sur l'écran.

![Julia image 1](/img/fractol/julia_1.png)

![Julia image 2](/img/fractol/julia_2.png)

J'ai lu beaucoup de sources sur les mathématiques des fractales et j'ai basé mon travail dessus. La fractale est un côté inspirant des mathématiques et peut être de l'art. J'ai découvert le Budhabroth, un mandelbrot géant aléatoire.

[Les sources d'inspiration (eng)](https://gitlab.com/husahuc/fractol/-/blob/master/sources.md)