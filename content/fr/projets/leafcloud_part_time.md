+++
title = "LeafCloud"
categories = "42 intership"
type_content = "code"
preview = "/img/preview/LeafCloud.png"
date = 2020-08-22T18:00:57+02:00
description = "Aider a construire un cloud ecologique"
type = "post"
[[tech]]
logo = "https://static.djangoproject.com/img/logos/django-logo-positive.png"
name = "Django"
url = "https://www.djangoproject.com"
[[tech]]
logo = "https://upload.wikimedia.org/wikipedia/commons/6/67/Kubernetes_logo.svg"
name = "Kubernetes"
url = "https://kubernetes.io"
[[tech]]
logo = "https://object-storage-ca-ymq-1.vexxhost.net/swift/v1/6e4619c416ff4bd19e1c087f27a43eea/www-images-prod/openstack-logo/OpenStack-Logo-Vertical.png"
name = "Openstack"
url = "https://www.openstack.org"
+++

Dans le cadre de mon cursus Codam Advanced aux Pays-Bas, j'ai effectué un stage à temps partiel chez LeafCloud.

LeafCloud est une nouvelle plate-forme de service de cloud comme AWS, mais beaucoup plus durable pour l'environnement. Au lieu de placer des serveurs dans de grands data-centers, LeafCloud place les serveurs dans des endroits nécessitant de la chaleur comme des résidences ou des hôtels. Pour l'instant, en réutilisant l'énergie du serveur pour réchauffer l'eau. Les serveurs fonctionnent sur OpenStack, une alternative open source largement utilisée.

J'ai aidé principalement à soutenir la plateforme utilisateur et le parcours client. Pour cela, nous avons utilisé Django pour gérer le backend, car c'est une solution simple en python pour implémenter des choses complexes comme une page d'administration. Python a également une extension qui s'intègre bien avec l'API Openstack.

J'ai mis en place une page de rapport pour voir tous les taux de calcul des utilisateurs au cours des derniers mois. Il permet désormais de voir les évolutions des plans et d'avoir une idée de l'utilisation totale et des revenus du cloud.

J'ai créé une page pour voir tous les nouveaux utilisateurs et leur source dans une journée donnée, pour aider le marketing à mieux suivre le client.

J'ai également aidé à automatiser le processus de facturation et de facturation. Le processus par lequel l'utilisateur doit payer pour l'utilisation d'un certain mois.

Au final, j'ai mis en place des scripts automatiques pour envoyer des mails, comme un mail à la fin de la période d'essai gratuite.

J'ai appris à utiliser Kubernetes et la plateforme Openstack. Et en général, comment une infrastructure de cloud fonctionne et est mise en œuvre.

J'utilise personnellement le service pour mon Gitlab et prévois de migrer tous mes projets de site Web et autres. Je promeus LeafCloud en France. Si vous souhaitez avoir un impact et faire fonctionner vos infrastructures dans le cloud, contactez-moi, je me ferai un plaisir de vous aider.