+++
title = "Malloc"
categories = "42 Cursus"
type_content = "code"
preview = "/img/preview/malloc.png"
date = 2020-08-22T16:30:07+02:00
description = "Ranger la memoire"
coders = []
status = ["done"]
gitlab = ["https://gitlab.com/husahuc/malloc"]
type = "post"
+++

Malloc est une fonction base pour chaque programmeur c pour allouer de la mémoire variable, comme un tableau d'entiers.

Pour comprendre en profondeur de comment fonctionne la mémoire, c'est une bonne idée d'examiner comment les commandes de gestion de la mémoire telles que malloc et free sont implémentées.

Il est nécessaire d'optimiser l'appel à la fonction système (nmap) pour les petites et moyennes allocations. Un des noyens est de préallouer une partie de la mémoire et n'appeler qu'une seule fois avec un gros et renvoyer à chaque fois une partie de cette mémoire.

Au premier appel, mon malloc alloue des tas de blocs pour les petits et moyens appels. Je stocke en mémoire dans une liste chaînée de heap (type de mémoire groupé) et à l'intérieur de celle-ci une liste chaînée de block. C'est facile de se déplacer dans la mémoire avec ça.

Pour chaque block, il y a un header dans la mémoire avec le pointeur vers le précédent block et le suivant, la taille de ce block, s'il est libre. C’est facile de calculer le pointeur à retourner puisque la taille de l'en-tête est fixe.

Malloc vérifiera la taille et trouvera un heap correct pour cette taille qui a encore des block libre. Il renverra l'adresse du bloc à la fin et le définira sur alloué.

La fragmentation peut se produire lorsque la mémoire est dispersée dans le tas. Pour éviter la fragmentation, malloc vérifie si le dernier ou le prochain bloc est libre et peut fusionner ces deux blocks.

Pour vérifier comment la mémoire est allouée au projet, il existe la commande show_alloc_mem qui affiche chaque heap et chaque block.

Il y a le script ./run.sh pour tester mon malloc dans le terminal. Chaque commande du shell utilisera mon malloc, y compris le shell lui-même.

Il existe des astuces pour faire face à toutes les situations. Pour travailler avec un programme spécifique comme Vim, la mémoire doit être alignée sur 16 bits.

[Les sources d'inspirations (eng)](https://gitlab.com/husahuc/malloc/-/blob/master/sources.md)