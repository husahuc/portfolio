+++
title = "Ft_hangouts"
categories = "42 Cursus"
type_content = "code"
preview = "/img/preview/ft_hangouts.png"
date = 2020-08-22T17:29:14+02:00
description = "Une simple application Android"
coders = []
status = ["done"]
gitlab = ["https://gitlab.com/husahuc/ft_hangouts"]
type = "post"
+++

Bienvenue dans le monde du développement d'applications mobiles, avec une expérience d’Android Studio lourde et beaucoup de nouvelles choses étranges.

J'ai fait une application Android simple, pour stocker des contacts et recevoir des messages. Avec une page pour ajouter un contact, une autre pour voir tous les contacts, et une pour envoyer un message à un contact.

La partie intéressante du développement d'applications mobile est que vous ne pouvez pas concevoir de pages en HTML/CSS et devez utiliser XML. Les l'activité en java sont utiles pour gérer toutes les interactions. Comme recevoir ou envoyer des messages.

Pour l'avenir, je prévois de le retravailler dans un développement piloté par les tests et de jeter un œil à Krotlin pour un code plus simple.