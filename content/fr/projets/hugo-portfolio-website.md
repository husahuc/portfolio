+++
categories = "Personnel"
type_content = "code"
coders = []
preview = "/img/preview/Portfolio.png"
date = 2020-06-19T23:00:00Z
description = "Un Portfolio fait avec Hugo"
github = ["https://gitlab.com/husahuc/portfolio"]
title = "Portfolio"
type = "post"
[[tech]]
logo = "https://res.cloudinary.com/samrobbins/image/upload/v1591793276/logos/logos_hugo_h2xbne.svg"
name = "Hugo"
url = "https://gohugo.io/"
[[tech]]
logo = "https://bulma.io/images/bulma-logo.png"
name = "Bulma"
url = "https://bulma.io"
[[tech]]
logo = "/img/Portfolio/gitlab-logo-gray-rgb.png"
name = "Gitlab Pages"
url = "https://about.gitlab.com/stages-devops-lifecycle/pages/"
+++

C'est le site que vous êtes actuellement en train de regarder. Il a été fait avec Hugo, et Bulma sur la base de hugo-developer-portfolio.

J'ai fait la transition sur Bulma parceque je trouve que c'est un framework css beaucoup plus simple que UiKit que le theme utilisait. Bulma a une excellente documentation, et des variables qui permettent de personnaliser le site selon mon identite, comme en changeant les couleurs et la police.