+++
title = "Generataure"
type_content = "code"
preview = ""
categories = "personal"
date = 2020-08-22T19:04:45+02:00
description = "Une application pour gérer des fictions interactives multijoueurs.euse"
gitlab = ["https://gitlab.com/husahuc/aventures"]
type = "post"
[[tech]]
logo = "https://www.django-rest-framework.org/img/logo.png"
name = "Django rest framework"
url = "https://www.django-rest-framework.org"
[[tech]]
logo = "/img/aventures/Discord-Logo-Black.png"
name = "Discord.py"
url = "https://discordpy.readthedocs.io/en/stable/"
[[tech]]
logo = "https://bulma.io/images/bulma-logo.png"
name = "Bulma"
url = "https://bulma.io"
+++

J'aime raconter des histoires et faire des aventures pour les autres. L'idée de cette application est venue en 2020 lorsque j'ai fait des histoires interactives multijoueurs avec plus de 10 personnes pendant deux mois.

Chaque jour, chaque personne recevra un message sur Discord pour faire un choix. Comme bouger ou interagir avec l'environnement. Chaque endroit est modifiable si vous y placez un objet, le prochain joueur le verra à cet endroit. Cela rend l'aventure interactive pour tout le monde et vos choix ont de réelles conséquences pour les autres joueurs.

J'ai utilisé Notion pour suivre les gens, où ils se trouvent et tout ce qui s'est passé ici. Mais Notion peut être lourd avec des utilisations très larges et spécifiques. Et j'ai toujours besoin d'envoyer des messages manuellement dans différentes applications.

Le besoin d'une application pour accélérer ce processus est venu rapidement. J'ai donc fait une application en Django avec Bulma pour le front-end.  L'organisation est centrée autour de campagnes qui regroupent tout.

- Les aventures sont des endroit ou quelque chose s'est passé, où un joueur fera un choix. Elles ont une description fixe et une note évolutive à leur sujet (conséquences des actions des joueurs...).
- Les personnages sont dans une l'aventure et peuvent suivre le joueur. Ils ont une description fixe (à quoi ressemble cette personne...), et une note (ex : souvenirs des joueurs rencontrés).
- Les objets sont dans une aventure ou peuvent suivre le joueur ou un personnage. Ils ont des descriptions fixes et une note évolutive (ex : dégradation de l'objet).

Avec ce système, je peux écrire en préparation tous les lieux/personnages/objets que je veux dans ma campagne. Et il suffit d'écrire la réaction des interactions des joueurs quand iels y jouent. Cela peut être utilisé pour une simple campagne de jeu de rôle. Le système n'est pas spécifique pour être ouvert à d'autres genres. Je pense faire une campagne de « civilisation ».

La partie délicate est l'intégration de Discord. J'ai choisi de créer un bot avec [discord.py](http://discord.py/). Puisque Django n'est pas asynchrone, le bot est dans son propre processus. l'application Django communique avec une API rest. Si le bot tombe en panne ou est trop lent, je peux simplement le relancer sans interrompre l'application Django.

Je prévois de le publier en open source et peut-être d'avoir plus d'intégrations comme la messagerie ou d'autres réseaux sociaux. J'écris une petite campagne pour tester le système dans mon univers, mais n'hésitez pas à me contacter si vous souhaitez rejoindre ou créer une nouvelle campagne.