+++
title = "Generataure"
type_content = "code"
preview = "/img/preview/Generataure.png"
categories = "Personnel"
date = 2020-08-22T19:04:45+02:00
description = "Un generateur de mots aléatoire"
gitlab = ["https://gitlab.com/husahuc/generator"]
type = "post"
[[tech]]
logo = "https://firebase.google.com/downloads/brand-guidelines/PNG/logo-built_white.png"
name = "Firebase"
url = "https://firebase.google.com"
[[tech]]
logo = "https://vuejs.org/images/logo.png"
name = "VueJS"
url = "https://vuejs.org"
[[tech]]
logo = "https://bulma.io/images/bulma-logo.png"
name = "Bulma"
url = "https://bulma.io"
+++

Pour l'écriture d'une aventure de jeu de rôle, j'ai eu l'idée de créer un outil pour générer des mots à partir d'association de syllabes. C'est juste aléatoire, mais il y a la possibilité d'associer des syllabes et de créer un style de langue, comme des origines communes.

Ca a été aussi l'occasion de revenir à VueJs pour faire une application web et d'utiliser Bulma et sa bonne documentation pour le front. J'ai pu découvrir Firebase, et son usage simple pour créer une base de donné en temps réel.

![Generataure image](/img/generataure/web.png)

[Generataure](https://generataure.web.app)