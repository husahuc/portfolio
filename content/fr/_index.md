---
banner:
  title: Coucou !
  intro: Je suis intéressé de développer des applications qui peuvent être utiles pour tous

social:
  linkedin: hugo-sahuc-70194b194
  gitlab: husahuc
  mail: sahuc.hugo@pro

competences:
  enable: true
  tiles:
    tile_1:
      text: Compétent et avec de l’expérience en  **C**, **C++**, **Javascript**, **Java**, **Python**.
            J’apprends actuellement **Rust**, **Android**, et **OpenGL**.
    tiles_1:
      - text: Vue.js est idéal pour créer des applications Web facilement et simplement en Javascript.
              J'ai réalisé plusieurs projets pour essayer de nouvelles fonctionnalités de Vue comme **Vue 3** et l'API de composition, **VueX**, **Nuxt**. 
        color: vue-color
      - text: Django vient avec toute la facilité d'utilisation de Python pour rendre le développement d'une application        complexe facile.
              J'utilise souvent **Django Rest Framework** comme base solide pour créer une API back end.
        color: django-color
    tile_2:
      text: Je déploie mes projets avec **Docker** et gère avec **Kubernetes**.
            Habitue a l'environnement **Linux** quotidiennement.
    tiles_2:
      - text: Promoteur d’un cloud plus écologique pour un avenir plus durable.
              Je peux vous aider à configurer et à migrer votre infrastructure avec **LeafCloud**.
        color: cloud-color
    tile_3:
      text: Une bonne organisation est la clé d'un travail réussi. Je planifie et suis mes tâches personnelles dans Notion.
            Git est mon  pour partager mon travail avec les autres.
            Je travaille mieux en groupe pour obtenir les meilleures idées et contributions de chaque personne sur le sujet.
            Une méthode de travail agile avec des objectifs clairs hebdomadaires est le bon moyen d'être plus productif chaque jour.
            J'ai utilisé **Miro** pour préparer des plans globaux.
            Je pense que **GitLab/GitHub** est mieux utilisé en groupe pour suivre les problèmes et faire une intégration continue.


about:
  enable: true
  resume:
    title: Mon cv
    url: /pdf/cv_Husahuc_fr.pdf
  education:
    - name: 42 Lyon
      sub: Old inner 42 core
      date: 2018 - 2020
      img: /img/education/42.svg
    - name: Codam
      sub: Codam Advanced
      date: 2020 - 2022
      img: /img/education/codam.svg

contact:
  enable: true
  formAction: "https://formspree.io/xjvadzvg"
---