+++
title = "Ft_hangouts"
categories = "42 Cursus"
type_content = "code"
preview = "/img/preview/ft_hangouts.png"
date = 2020-08-22T17:29:14+02:00
description = "Android application hell"
coders = []
status = ["done"]
gitlab = ["https://gitlab.com/husahuc/ft_hangouts"]
type = "post"
+++

Welcome to the world of mobile app development, with a slow android studio experience and a lot of new strange stuff.

I did a simple android app, to store contacts and receive messages. With a page to add a contact, another to see all the contact, and one to send a message to a contact.

The interesting part of mobile app development is that you can not design pages in HTML/CSS and have to use XML. You code activity in java to deal with all the interactions and the phone ecosystem. Like sending a message or receiving a message.

For the future, I plan to rework it in a completely test-driven development and have a look at Krotlin for simpler code.