+++
title = "LeafCloud"
categories = "42 intership"
type_content = "code"
preview = "/img/preview/LeafCloud.png"
date = 2020-08-22T18:00:57+02:00
description = "Help build a sustainable green cloud service"
type = "post"
[[tech]]
logo = "https://static.djangoproject.com/img/logos/django-logo-positive.png"
name = "Django"
url = "https://www.djangoproject.com"
[[tech]]
logo = "https://upload.wikimedia.org/wikipedia/commons/6/67/Kubernetes_logo.svg"
name = "Kubernetes"
url = "https://kubernetes.io"
[[tech]]
logo = "https://object-storage-ca-ymq-1.vexxhost.net/swift/v1/6e4619c416ff4bd19e1c087f27a43eea/www-images-prod/openstack-logo/OpenStack-Logo-Vertical.png"
name = "Openstack"
url = "https://www.openstack.org"
+++

As a part of my Codam Advanced curriculum in the Netherlands, I did a part-time at LeafCloud.

LeafCloud is a new cloud platform like AWS, but far more sustainable for the environment. Instead of putting servers in big data centers, LeafCloud put the servers in places in need of heat like residences or hotels. For now, by reusing the energy of the server to warm the water. The servers run on OpenStack, an open-source alternative used widely.

I helped primarily to support the user platform and the customer journey. For that, we used Django to manage the backend because it's an easy solution in python to implement complex things like an admin page. Python has also an extension that integrates well with the Openstack API.

I implemented a reporting page to see all the compute rates of the users in the past few months. It's used now to see the evolutions of the plans and have an idea of the total usage and the revenue of the cloud.

I made a page to see all the new users and the source of it in a given day, to help marketing better track the customer.

I also helped automatize the process of billing and invoicing. The process for the user to pay for the usage of a certain month.

In the end, I implemented automatic scripts to send mail during the customer journey, like a mail at the end of the free trial period.

I learned how to use Kubernetes and the Openstack platform. And in general how a cloud system work and is implemented.

I use the service personally for my Gitlab and plan to migrate all my website and others projects. If you want to make an impact and run your infrastructures in the cloud, contact me, I would gladly help.