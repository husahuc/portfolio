+++
title = "Ft_ls"
categories = "42 Cursus"
type_content = "code"
preview = "/img/preview/ft_ls.png"
date = 2020-08-22T16:30:07+02:00
description = "Recode the ls command to learn about system"
coders = []
status = ["done"]
gitlab = ["https://gitlab.com/husahuc/ft_ls"]
type = "post"
+++

It was one of the first real big projects that I did on my curriculum. Recode the Ls command is about a real organization in the code to finish this project. Start on a good path and follow it.

![ft_ls image](/img/ft_ls/ft_ls_1.png)

The tricky part is the -R option to list with recursion, see the subfolder and file of a folder. Also, to see the special files, like the /dev directory and its special devices files. And the alignment of all to think fist in the code.

![ft_ls image](/img/ft_ls/ft_ls_2.png)

There are some errors in the code, like the sort with multiples options. I could also use a quick sort algorithm.

![ft_ls image](/img/ft_ls/ft_ls_3.png)