+++
title = "Simple vote"
categories = "personal"
type_content = "code"
preview = "/img/preview/poll_simple.png"
date = 2020-08-22T17:29:14+02:00
description = "A simple poll app"
coders = []
status = ["done"]
gitlab = ["https://gitlab.com/husahuc/poll"]
type = "post"
+++

I plan to make a bigger app that allows to vote and participate in creating projects. For this, I wanted to resume coding and learn new tech. I simply coded a poll app for myself.

The choice of backend is very difficult with all the solutions, and I want to test some more niche tech to compare. I already coded with Django rest framework but it was too complex for this simple project. So I explored FastAPI, a Python framework with uses s native python features. It is very fast and intuitive to create an API, and the automatic doc web interface is a great view of all the API entry. I use Python SQLarchemy for database creation and relations because the doc example uses it, but FastApi can use other database models.
I learned with this tutorial: https://www.agiliq.com/blog/2020/05/polls-api-using-fastapi/

For keeping up with the new tech, I used Vue 3 because I already know Vue very well. Vue 3 and the composition API change organization of a Vue project, with separate code based on the data to simplify the component page. Buefy is of course my UI framework of choice based on Bulma for Vue.
This article explains well all the new features in Vue3: https://littlelines.com/blog/2020/06/03/vue-3-introduction-and-overview

![Poll image home](/img/Poll_simple/polls_1.png)
![Poll image vote](/img/Poll_simple/polls_2.png)
![Poll image edit](/img/Poll_simple/polls_3.png)