+++
categories = "personal"
type_content = "code"
preview = "/img/preview/Portfolio.png"
coders = []
date = 2020-06-19T23:00:00Z
description = "A portfolio website made with Hugo"
github = ["https://gitlab.com/husahuc/portfolio"]
title = "Portfolio Website"
type = "post"
[[tech]]
logo = "https://res.cloudinary.com/samrobbins/image/upload/v1591793276/logos/logos_hugo_h2xbne.svg"
name = "Hugo"
url = "https://gohugo.io/"
[[tech]]
logo = "https://bulma.io/images/bulma-logo.png"
name = "Bulma"
url = "https://bulma.io"
[[tech]]
logo = "/img/Portfolio/gitlab-logo-gray-rgb.png"
name = "Gitlab Pages"
url = "https://about.gitlab.com/stages-devops-lifecycle/pages/"
+++

This is the website you are currently seeing. It was first made using Hugo and Bulma on the base of Hugo-developer-portfolio.

I made the transition to Bulma because I find it much simpler than UiKit that it uses. Bulma has great documentation, and variable allows to personalize the sites to fulfill my identity, like changing colors and the fonts.