+++
title = "Generataure"
type_content = "code"
preview = "/img/preview/Generataure.png"
categories = "personal"
date = 2020-08-22T19:04:45+02:00
description = "A random word generator"
gitlab = ["https://gitlab.com/husahuc/generator"]
type = "post"
[[tech]]
logo = "https://firebase.google.com/downloads/brand-guidelines/PNG/logo-built_white.png"
name = "Firebase"
url = "https://firebase.google.com"
[[tech]]
logo = "https://vuejs.org/images/logo.png"
name = "VueJS"
url = "https://vuejs.org"
[[tech]]
logo = "https://bulma.io/images/bulma-logo.png"
name = "Bulma"
url = "https://bulma.io"
+++

For a roleplaying adventure, I have an idea to create a tool to generate words based on the association of syllable. It is just random, but it has the possibility of association of selected syllables and to create a style of a word, like the same origin.

It was also the occasion to revisit VueJs for a web app and use Bulma and its good documentation for the css. And the discovery of Firebase, its simplicity to create an auth based back-end.

![Gererataure image](/img/generataure/web.png)

[Generataure](https://generataure.web.app)