+++
title = "Rte"
categories = "42 intership"
type_content = "code"
preview = "/img/preview/RTE-CDF.png"
date = 2020-08-22T18:00:57+02:00
description = "Following an learning app at Rte"
type = "post"
[[tech]]
logo = "https://www.django-rest-framework.org/img/logo.png"
name = "Django rest framework"
url = "https://www.django-rest-framework.org"
[[tech]]
logo = "https://vuejs.org/images/logo.png"
name = "VueJS"
url = "https://vuejs.org"
[[tech]]
logo = "https://avatars1.githubusercontent.com/u/22965283?s=200&v=4"
name = "Bootstrap Vue"
url = "https://bootstrap-vue.org"
+++

I did my first internship for the 42 curriculum at Rte, to follow a learning follow up app.
Rte is responsible for the operation, maintenance, and development of the High-Voltage lines in France.

Rte, due to it specificity has a very big internal learning center with 10 % dedicated to it. 
To follow the interns, the app "Cahier de formation" was made. It allows the trainers to evaluate and have a global vision of the progress of the apprentices. The app has multiple types of evaluations, like Quizzes or put in practice resume.

My mission was to migrate that app in a new simpler version. The back was made with Django rest framework for the api calls. We considered Angular for a few weeks, for its complete solution. But we finally used VueJs, because it is much simpler. I have seen all the important aspects of Vue, like the router for pages, VueX for the variables. Bootstrap Vue was used for the css, because it was simple and have a good documentation. I finally did some unit tests to checks its all works with Vue-test-utils.

Due to the pandemic taking place, we used Teams to have weekly meetings call. We use Git to share advancement.

The final version of the app in production, with training happening. The users are happy with the increased fluidity. The code is simpler, with better-separated modules, allowing for a better follow up.

The video that I made for my internship with subtitles:

{{< dailymotion x7wd1pj >}}