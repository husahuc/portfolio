+++
title = "Fractol"
categories = "42 Cursus"
type_content = "code"
preview = ""
date = 2020-08-22T16:30:07+02:00
description = "Cool fractal"
coders = []
status = ["done"]
gitlab = ["https://gitlab.com/husahuc/fractol"]
type = "post"
+++

Fract-ol is one of the famous 42 projects, a good introduction to graphics programming and how to deal with strange mathematics.

A fractal is a pattern that never ends often based on a mathematical formula. They are self-similar across different scales with means that you can zoom in and see the same pattern again and again. They can be created by repeating a simple process over and over in an ongoing feedback loop, in code with recursion.

You can display images of a fractal ensemble with the real part of the complex number as a displacement along the x-axis and the imaginary part as a displacement along the y-axis. Calculate for each point the number of iteration in the sequence and check if it is finite or infinite, and the number of iterations.

There are options to change the range of colors depending on the number of iterations, you can get pretty psycho about it.

You can zoom in and click to center on the pointer. There is an option to augment the number max of iterations gives more details to the schapes. The process of calculating each pixel takes a lot to calculate and you can divide that process. Hence the need for multithreading, calculating multiple points with different threads. I choose to have 4 threads, each calculating pixel in every corner of the screen.

**Mandelbrot** is the set of complex numbers for which the function f(x) = z*2 + c  does not diverge to infinity when iterated from f(x) = 0. It can be interesting to see every ‘corner’ and the ‘ray’ to get a circular pattern.

![mandelbroth image 1](/img/fractol/mandelbroth_1.png)

![mandelbroth image 2](/img/fractol/mandelbroth_2.png)


**Burning ship** is the set of absolute values of mandelbrot. You can zoom in to see different types of ‘burning ship’.

![Burning ship image 1](/img/fractol/burning_ship_1.png)

**Julia** Set Fractal represents the set of inputs whose resulting outputs either tend towards infinity or remain bounded, in the operation z = z2 + c. You can change the z and x values by moving the cursor on the screen.

![Julia image 1](/img/fractol/julia_1.png)

![Julia image 2](/img/fractol/julia_2.png)

I read a lot of sources about the mathematics of fractals and based my work on it. Fractal is an inspiring side of mathematics and can be art. I discovered the Budhabroth, a random giant mandelbrot.

[The sources](https://gitlab.com/husahuc/fractol/-/blob/master/sources.md)