+++
title = "Malloc"
categories = "42 Cursus"
type_content = "code"
preview = "/img/preview/malloc.png"
date = 2020-08-22T16:30:07+02:00
description = "Memory dealing"
coders = []
status = ["done"]
gitlab = ["https://gitlab.com/husahuc/malloc"]
type = "post"
+++

Malloc is a basic for every c programmer to allocate variable memory, like an array of int.

To understand in the deep level how memory works, it’s a good idea to have a look at how the memory management command like malloc and free are implemented.

There is a need to optimize the call to system function (nmap) for small and medium allocation. you can ‘pre-allocate some part of memory and only call once with a big and every time return a part of that memory.

At the first call, my malloc allocates heap of blocks for small and medium. I store in the memory in a chained list of heap (grouped type of memory) and inside it a chained list of block. It's easy to move in the memory like that.

For every block, there is a header in the memory with the pointer to the last and next block, the size of that block, if it’s free and you can calculate the pointer to return since it’s fixed size of the header.

Malloc will check the size and find a correct heap for that size that still has some block free. It will return the address of the block a the end and set it to allocated.

Fragmentation can happen when memory is scattered in the heap, to avoid fragmentation malloc checks if the last or next block is free and can merge those two block

To check how the memory is allocated for the project, there is the show_alloc_mem command that shows every heap and block.

There is the ```./run.sh``` to test my malloc in the terminal. Every command in the shell will use my malloc, including the shell itself.

There are some tricks to deal with all the situations. To work with a specific program like Vim, the memory needs to be aligned on 16 bit.

[The sources](https://gitlab.com/husahuc/malloc/-/blob/master/sources.md)