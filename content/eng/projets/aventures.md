+++
title = "Generataure"
type_content = "code"
preview = ""
categories = "personal"
date = 2020-08-22T19:04:45+02:00
description = "App to manage a multiplayer interactive story"
gitlab = ["https://gitlab.com/husahuc/aventures"]
type = "post"
[[tech]]
logo = "https://www.django-rest-framework.org/img/logo.png"
name = "Django rest framework"
url = "https://www.django-rest-framework.org"
[[tech]]
logo = "/img/aventures/Discord-Logo-Black.png"
name = "Discord.py"
url = "https://discordpy.readthedocs.io/en/stable/"
[[tech]]
logo = "https://bulma.io/images/bulma-logo.png"
name = "Bulma"
url = "https://bulma.io"
+++

I like to tell stories and make adventures for others. The idea for this app came in 2020 when I did multiplayer interactive stories with more than 10 people for two months.

Everyday day, each person will receive a message on Discord to make a choice. Like moving, or interacting with the environment. Every place is changeable if you place an object there, the next player will see it in this place. It makes the adventure interactive by everyone and your choices have real consequences to others players.

I used Notion to track people, where they are, and all the things that happened here. But Notion can be slow and delicate with very large and specific uses. And I still need to send messages manually in different apps.

The need for an app to make this process faster came. I made an app in Django with Bulma for the front-end. I centered the organization around campaigns that regroup everything.

- ‘Adventures’ are a place or something that happened, where a player will make a choice. they have a fixed description and an evolutive note about it (consequences of the actions).
- ‘Characters’ are on adventures and can follow the player. They have a fixed description (what this person loke like...), and a note (ex: memories of the players they meet).
- ‘Objects’ are in an adventure or can follow the player or a character. They have fixed descriptions and an evolutive note (ex: damage to the object).

With this system, I can write all the places/characters/objects that I want in my campaign in preparation. And just write the reaction of the interactions of the players playing it. It can be used for a simple roleplaying campaign. The system is non-specific to be open for other genres. I am thinking about doing a “civilization” campaign.

The tricky part is the Discord integration. I choose to make a bot with [discord.py](http://discord.py/). Since Django is not Asynchronous, the bot is in its own process. the Django app communicates with a rest API. If the bot broke or is too slow, I can simply relaunch it without interrupting the Django app.

I plan to release it opensource and maybe have more integrations like mail or other social networks. I am writing a small campaign to test the system in my universe but feel free to contact me if you want to join or create a campaign.