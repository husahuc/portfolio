---
banner:
  title: Hi! I’m Hugo, a French developper.
  intro: I am interessed in developping apps that can be usefull.

social:
  linkedin: hugo-sahuc-70194b194/?locale=en_US
  gitlab: husahuc
  mail: sahuc.hugo@pro

competences:
  enable: true
  tiles:
    tile_1:
      text: Proficient and with experience in **C**, **C++**, **Javascript**, **Java**, **Python**.
            Currently, I am learning **Rust**, **Android**, and **OpenGL**.
    tiles_1:
      - text: Vue.js is great to make web apps easily and simply in Javascript.
              I made multiple projects to try new features of Vue like **Vue 3** and the composition API, **VueX**, **Nuxt**.
        color: vue-color
      - text: Django comes with all the ease of use of Python to make the development of a complex app trivial.
              I often use **Django Rest Framework** as a solid foundation to make an API for the external front end.
        color: django-color
    tile_2:
      text: I deploy my projects using **Docker** containers and orchestrate with **Kubernetes**.  
            I use the **Linux** environment daily.
    tiles_2:
      - text: Green cloud promoter for a more sustainable future.
              I can help you set up and migrate your infrastructure to the cloud in LeafCloud.
        color: cloud-color
    tile_3:
      text: Good organization is key to successful work.  
            I plan and track my personal tasks with **Notion**.
            Git is my go-to to share my work with others.
            Work well in a group to get the best ideas and input of every person in the matter.  
            Agile workflow with weekly goals is the way to get a productive day for me.
            I used **Miro** to prepare global plans
            I think that **GitLab/GitHub** is best used in groups to keep track of the issues and do continuous integration.


about:
  enable: true
  resume:
    title: My resume
    url: /pdf/cv_Husahuc_eng.pdf
  education:
    - name: 42 Lyon
      sub: Old inner 42 core
      date: 2018 - 2020
      img: /img/education/42.svg
    - name: Codam
      sub: Codam Advanced
      date: 2020 - 2022
      img: /img/education/codam.svg


projects:
  enable: true
  list:
    - 42sh
    - Rte

contact:
  enable: true
  formAction: "https://formspree.io/xjvadzvg"
---